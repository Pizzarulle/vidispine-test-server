package com.example.vidispinecodetestserver.Mandelbrot;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class MandelbrotSet {

    /**
     * Generates a Mandelbrot Set and returns a {@link BufferedImage}
     * @param parentWidth width of the original image
     * @param parentHeight height of the original image
     * @param maxIterations max iterations
     * @param startIndex starting position
     * @param endIndex stop position
     * @param currentMaxHeight height of the img that's going to be created
     * @return {@link BufferedImage}
     * @throws IOException
     */
    public BufferedImage create(int parentWidth, int parentHeight, int maxIterations, int startIndex, int endIndex, int currentMaxHeight) throws IOException {

        BufferedImage image = new BufferedImage(parentWidth, endIndex, BufferedImage.TYPE_INT_RGB);

        int black = 0;
        int[] colors = new int[maxIterations];
        for (int i = 0; i<maxIterations; i++) {
            colors[i] = Color.HSBtoRGB(i/256f, 1, i/(i+8f));
        }

        for (int row = startIndex; row < endIndex; row++) {
            for (int col = 0; col < parentWidth; col++) {

                double c_re = (col - parentWidth / 2) * 4.0 / parentWidth; // real number
                double c_im = (row - parentHeight / 2) * 4.0 / parentWidth; // imaginary number

                double x = 0;
                double y = 0;
                int iterations = 0;
                while (x * x + y * y < 4 && iterations < maxIterations) {
                    double x_new = x * x - y * y + c_re;
                    y = 2 * x * y + c_im;
                    x = x_new;
                    iterations++;
                }
                if (iterations < maxIterations) image.setRGB(col, row, colors[iterations]);
                else image.setRGB(col, row, black);
            }
        }

        return image.getSubimage(0, startIndex, parentWidth, currentMaxHeight);
    }
}
