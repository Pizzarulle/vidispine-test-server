package com.example.vidispinecodetestserver.Mandelbrot;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class MandelbrotDTO {

    private int orderId;
    private byte[] imageInByte;
    private String error;

    public MandelbrotDTO() {
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public byte[] getMandelbrotImage() {
        return imageInByte;
    }
    //Converts a BufferedImage to ByteArray
    public void setMandelbrotImage(BufferedImage mandelbrotImage) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(mandelbrotImage, "png", baos);
        baos.flush();
        this.imageInByte = baos.toByteArray();
        baos.close();
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
