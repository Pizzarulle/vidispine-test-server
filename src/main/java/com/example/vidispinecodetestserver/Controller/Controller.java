package com.example.vidispinecodetestserver.Controller;

import com.example.vidispinecodetestserver.Mandelbrot.MandelbrotDTO;
import com.example.vidispinecodetestserver.Mandelbrot.MandelbrotSet;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class Controller {
    /**
     * REST endpoints that takes in the variables that's needed to create a full/subImage of the mandelbrot set.
     *
     * @param maxIterations int number of the maximum amount f iterations
     * @param x width of the whole image
     * @param y height of the whole image
     * @param startIndex start index for the sub-image
     * @param endIndex end index for the sub-image
     * @param currentMaxImgHeight height of the sub-image
     * @param orderId int value for keeping track of wich order the api calls have been made
     * @return byte-array
     * @throws IOException
     */
    @GetMapping("/mandelbrot/{maxIterations}/{x}/{y}/{startIndex}/{endIndex}/{currentMaxImgHeight}/{orderId}")
    public byte[] getMandelbrot(
            @PathVariable int maxIterations,
            @PathVariable int x,
            @PathVariable int y,
            @PathVariable int startIndex,
            @PathVariable int endIndex,
            @PathVariable int currentMaxImgHeight,
            @PathVariable int orderId) throws IOException {

        MandelbrotSet mb = new MandelbrotSet();
        MandelbrotDTO dto = new MandelbrotDTO();

        try {
            dto.setMandelbrotImage(mb.create(x, y, maxIterations, startIndex, endIndex, currentMaxImgHeight));
        } catch (Exception e) {
            e.printStackTrace();
            dto.setError(e.getMessage());
        } finally {
            dto.setOrderId(orderId);
        }

        //Should return the whole dto-object but trouble on the client side
        return dto.getMandelbrotImage();
    }
}
