package com.example.vidispinecodetestserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VidispineCodeTestServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(VidispineCodeTestServerApplication.class, args);
    }

}
