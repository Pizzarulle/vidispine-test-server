# Vidispine-test-server

RESTful WebServer used to create and return a full image/sub-image of the Mandelbrot Set.

# Dependencies
* JDK (to run project in IDE)
* Java IDE (IntelliJ of any other modern Java IDE)

# Install
* Clone project
* Open project in IDE and build

# Use 
* In ide run main method 

# API 
```
GET /mandelbrot/:maxIterations/:x/:y/:startIndex:/:endIndex/:currentMaxImgHeight/:orderId
```
